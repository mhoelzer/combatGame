function Character(characteristics) {
    if(!characteristics.name) {
        this.name = `Unnamed Hero #${Math.floor(Math.random() * 201)}`;
    } else {
        this.name = characteristics.name;
    };
    this.health = characteristics.health || 50;
    this.maxHealth = this.health * 2;
    this.normalDamage = characteristics.normalDamage || 10;
    this.chanceForMaxDamage = characteristics.chanceForMaxDamage || .1;
    this.chanceForMissing = characteristics.chanceForMissing || .35;
    this.color = characteristics.color;
};

const hero = new Character({
    name: prompt("Enter your name here:"),
    health: 250,
    chanceForMissing: .3,
});
const orangeFurby = new Character({
    name: `Orange "El Jefe" Furby`,
    health: 100,
    normalDamage: 20,
    chanceForMaxDamage: .7,
    chanceForMissing: .65
});
const magentaFurby = new Character({
    name: "Magenta Furby",
    chanceForMissing: .5
});
// const rainbowFurby = new Character({
//     name: "Rainbow Furby",
//     health: 77,
//     normalDamage: 77,
//     chanceForMaxDamage: .77,
//     chanceForMissing: .77
// });
const tigerFurby = new Character({
    name: "Tiger Furby",
    health: 65
});


Character.prototype.backgroundInfo = function() {
    let div = document.createElement("div");
    let backgroundInfo = `Hello, ${hero.name}. You are about to face the most horrific beings of all: the Furbies. You are at a strong ${hero.health} health, but the Furbies are strong as well: ${magentaFurby.name} has ${magentaFurby.health} health, ${tigerFurby.name} has ${tigerFurby.health} health, and ${orangeFurby.name} has ${orangeFurby.health} health. Be brave, be bold, and be the best.`
    let printMessage = document.createTextNode(backgroundInfo);
    div.appendChild(printMessage);
    div.style.border = "2px solid blue";
    div.style.fontSize = "20px";
    document.body.appendChild(div);
}
hero.backgroundInfo()


Character.prototype.battle = function(opponent) {
    let message;
    if(Math.random() < this.chanceForMissing) {
        message = `${this.name} missed ${opponent.name}!`;
    } else {
        const damage = Math.random() < this.chanceForMaxDamage ? this.normalDamage * 2 : this.normalDamage;
        opponent.health -= damage;
        message = `${opponent.name} was hit! ${opponent.name} now has ${opponent.health} health.`;
    }
    let div = document.createElement("div");
    let printMessage = document.createTextNode(message)
    div.appendChild(printMessage);
    document.body.appendChild(div);
    return message;
};

Character.prototype.deathMatch = function(...furbies) {
    furbies.forEach(furby => {
        while (this.health > 0 && furby.health > 0) {
            this.battle(furby);
            furby.battle(this);
        }
        if(this.health > 0) {
            victor = `${this.name} is the victor!`;
        } else if(furby.health > 0) {
            victor = `${furby.name} is the victor! :(`;
        } else if(this.health <= 0 && furby.health <= 0) {
            victor = `${this.name} fought valiantly but was slain; however, the Furbies have been destroyed. ${this.name}'s sacrifice was not in vain.`
        }
        // health updates
        healthMessage = `${this.name} has ${this.health} health, and ${furby.name} has ${furby.health} health.`;
        let div = document.createElement("div");
        let result = document.createTextNode(healthMessage);
        div.appendChild(result);
        div.style.border = "1px dashed lime";
        document.body.appendChild(div);
        
        // winner update 
        let divWin = document.createElement("div");
        let resultWin = document.createTextNode(victor);
        divWin.appendChild(resultWin);
        divWin.style.border = "3px dashed green";
        document.body.appendChild(divWin);
    })
}
// hero.deathMatch(magentaFurby, tigerFurby, rainbowFurby, orangeFurby);
hero.deathMatch(magentaFurby, tigerFurby, orangeFurby);